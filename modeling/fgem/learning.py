import numpy as np
from sklearn import linear_model
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
import os

import modeling_analysis

# Implement the target software system
import learn_kanzi as modeling_target_system

#  Set working directory to the directory that contains the measurements of a slurm job array.
#  This folder should contain the array numbered directories of the configs
#  with each containing the results/ directory.
measurements_dir = '/home/kronfeld/measurements/curie01/884562/'
# measurements_dir = os.getcwd().replace('\\', '/') + '/measurements/884562/'


def learn(directory) -> (list, list):
    # Learning energy and performance influence models
    measurements = modeling_target_system.parse_config_measurements(directory)
    methods_x_and_y = {}
    for config in measurements:
        for index, row in config.energy.iterrows():
            method_name = row['method']
            if method_name not in methods_x_and_y.keys():
                methods_x_and_y[method_name] = {'x_t': [], 'y_t': [], 'x_e': [], 'y_e': []}
            methods_x_and_y[method_name]['x_e'].append(
                modeling_target_system.get_config_attributes(config))
            methods_x_and_y[method_name]['y_e'].append(row['e'])
        for index, row in config.times.iterrows():
            method_name = row['method']
            if method_name not in methods_x_and_y.keys():
                methods_x_and_y[method_name] = methods_x_and_y[
                    method_name] = {'x_t': [], 'y_t': [], 'x_e': [], 'y_e': []}
            methods_x_and_y[method_name]['x_t'].append(
                modeling_target_system.get_config_attributes(config))
            methods_x_and_y[method_name]['y_t'].append(row['t'])

    method_models = []
    for method_name in methods_x_and_y.keys():
        e_model, e_errors, t_model, t_errors = 0, 0, 0, 0
        if len(methods_x_and_y[method_name]['x_e']) > 1:
            e_model, e_errors = fit_model(methods_x_and_y[method_name]['x_e'],
                                          methods_x_and_y[method_name]['y_e'])
        if len(methods_x_and_y[method_name]['x_t']) > 1:
            t_model, t_errors = fit_model(methods_x_and_y[method_name]['x_t'],
                                          methods_x_and_y[method_name]['y_t'])
        method_models.append({
            'method': method_name,
            'energy_model': e_model,
            'energy_errors': e_errors,
            'performance_model': t_model,
            'performance_errors': t_errors
        })
    return measurements, method_models


def fit_model(x, y):
    model = linear_model.Lasso(alpha=0.05)
    poly = PolynomialFeatures(2, interaction_only=True, include_bias=True)
    ppl = Pipeline([('poly', poly), ('clf', model)])
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    model = ppl.fit(x_train, y_train)
    y_pred = list(ppl.predict(x_test))
    errors = {
        'Mean Absolute': metrics.mean_absolute_error(y_test, y_pred),
        'Mean Absolute Percentage': mape_zero_like(y_test, y_pred)
    }
    return model, errors


def wmape(y_true, y_pred):
    # Weighted Mean Ansolute Percente Error
    diff = 0.0
    total = 0.0
    for i in range(len(y_true)):
        diff += np.absolute(y_true[i] - y_pred[i])
        total += np.absolute(y_true[i])
    return 100 * diff / total


def mape_zero_like(y_true, y_pred):
    # Alternative for - wmape(y_test, y_pre)
    #                 - metrics.mean_absolute_percentage_error(y_test, y_pred)
    # Avoids division by 0 problem.
    truth = np.array(y_true)
    prediction = np.array(y_pred)
    values_abs = np.abs(truth - prediction)
    values_div = np.divide(values_abs, truth, out=np.zeros_like(values_abs), where=truth != 0)
    return np.mean(values_div)


if __name__ == '__main__':
    # Configure working directory first!
    config_measurements, models = learn(measurements_dir)
    # ANALYSIS
    modeling_analysis.print_models(models=models)
    modeling_analysis.analyse_results(models=models)
    modeling_analysis.analyse_outliers(config_measurements=config_measurements, models=models)
