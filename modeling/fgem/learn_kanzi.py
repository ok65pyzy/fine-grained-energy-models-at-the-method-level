import os
import pandas as pd


class KanziConfig:
    def __init__(self, l, b, x, s, energy, times):
        self.l: int = l
        self.b: int = b
        self.x: int = x
        self.s: int = s
        self.energy = energy
        self.times = times

    def __str__(self):
        return 'kanzi-config_l=' + str(self.l) + '_b=' + str(self.b) + '_x=' + str(self.x) + '_s=' + str(self.s)


def parse_config_measurements(cwd) -> list:
    config_measurements = []
    max_b = 0.0
    # put in amount of measured configurations
    for i in range(1, 141, 1):
        try:
            df_e = pd.read_csv(cwd + str(i) + "/results/energy_result.csv", sep=';', usecols=['method', 'e'])
            df_t = pd.read_csv(cwd + str(i) + "/results/times_result.csv", sep=';', usecols=['method', 't'])
        except FileNotFoundError:
            print("not present", cwd + str(i) + "/results/*")
            continue
        dir_list = os.listdir(cwd + str(i) + "/energy/")
        parameter_file = cwd + str(i) + "/energy/" + dir_list[0] + '/parameter.txt'
        with open(parameter_file) as f:
            first_line = f.readlines()[0]
            params = first_line.split("', '")
            f.close()
        array_i = 0
        l, b, s, x = -1, -1, 0, 0
        for param in params:
            if param == '-l':
                l = int(params[array_i + 1])
            elif param == '-b':
                b = float(parse_number(params[array_i + 1]))
                if b > max_b:
                    max_b = b
            elif param == '-s':
                s = 1
            elif param == '-x':
                x = 1
            array_i += 1
        config = KanziConfig(l=l, b=b, x=x, s=s, energy=df_e, times=df_t)
        config_measurements.append(config)
        if l == -1 or b == -1:
            raise Exception("Invalid config: " + str(config))
    # normalize numeric option to fit on interval [0..1]
    for raw_config in config_measurements:
        raw_config.b = raw_config.b / max_b
    return config_measurements


def get_config_attributes(config: KanziConfig) -> list:
    attributes = []
    if config.l == 0:
        attributes.extend([1, 0, 0, 0, 0, 0, 0, 0, 0])
    elif config.l == 1:
        attributes.extend([0, 1, 0, 0, 0, 0, 0, 0, 0])
    elif config.l == 2:
        attributes.extend([0, 0, 1, 0, 0, 0, 0, 0, 0])
    elif config.l == 3:
        attributes.extend([0, 0, 0, 1, 0, 0, 0, 0, 0])
    elif config.l == 4:
        attributes.extend([0, 0, 0, 0, 1, 0, 0, 0, 0])
    elif config.l == 5:
        attributes.extend([0, 0, 0, 0, 0, 1, 0, 0, 0])
    elif config.l == 6:
        attributes.extend([0, 0, 0, 0, 0, 0, 1, 0, 0])
    elif config.l == 7:
        attributes.extend([0, 0, 0, 0, 0, 0, 0, 1, 0])
    elif config.l == 8:
        attributes.extend([0, 0, 0, 0, 0, 0, 0, 0, 1])
    else:
        raise Exception('invalid kanzi level: ' + str(config.l))
    attributes.append(float(config.b))
    attributes.append(config.x)
    attributes.append(config.s)
    return attributes


def parse_number(raw: str) -> int:
    return int(raw.replace('[', '').replace(']', '').replace(',', '').replace("\'", '').replace(")", ''))
