import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import os

pwd = os.getcwd().replace('\\', '/')
cwd = pwd + '/measurements/882905/'


def read_kanzi_time(job):
    slurm_task_id = str(job['i'])
    directory_name = cwd
    in_file = directory_name + slurm_task_id + "/traces/traces.log"
    dir_list = os.listdir(directory_name + slurm_task_id + "/energy/")
    energy_file = directory_name + slurm_task_id + "/energy/" + dir_list[0] + "/energy_measurements.csv"

    file = open(in_file, "r")
    list_of_lines = file.readlines()
    file.close()

    kanzi_time = -1
    for line in list_of_lines:
        if "Encoding" in line and "=>" in line:
            kanzi_time = line.split()[-2]
        if "-l" in line and "-b" in line:
            kanzi_l: int = int(line.split('-l')[1].split()[0])
            kanzi_b: int = int(line.split('-b')[1].split()[0])
            print(kanzi_l, kanzi_b)
            kanzi_x = 0
            if '-x' in line:
                kanzi_x = 1
            kanzi_s = 0
            if '-s' in line:
                kanzi_s = 1

    df = pd.read_csv(energy_file, sep=',', usecols=['t', 'val'])
    df['val'][0] = "0"

    e = df['val'].sum()

    print("total_time", kanzi_time)
    print("energy", e)
    config_data = np.zeros((1,), dtype=[('l', 'i8'), ('b', 'i8'), ('s', 'i8'), ('x', 'i8'), ('t', 'i8'), ('e', 'f8')])
    config_data[0] = (kanzi_l, kanzi_b, kanzi_s, kanzi_x, kanzi_time, e)
    return config_data


def compute_energy():
    t_e_map = np.array([], dtype=[('t', 'i8'), ('e', 'f8')])
    configs = np.array([], dtype=[('l', 'i8'), ('b', 'i8'), ('s', 'i8'), ('x', 'i8'), ('t', 'i8'), ('e', 'f8')])
    for i in range(1, 140, 1):
        data = read_kanzi_time({'i': i})
        configs = np.append(configs, data)
        # e-t-graph:
        t = data[0]['t']
        e = data[0]['e']
        t_e_map.resize(t_e_map.shape[0] + 1)
        t_e_map[-1] = (t, e)

    df = pd.DataFrame(data=configs)
    print(df)
    df.to_csv(cwd + "energy_influence.csv", index=False, sep=';')


if __name__ == '__main__':
    compute_energy()
