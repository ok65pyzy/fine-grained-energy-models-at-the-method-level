import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ast
from scipy.stats import variation

import learning

# Implement the target software system
import learn_kanzi as modeling_target_system

#  Set the working directory where the measurement data is located
# measurements_dir = '/u/kronfeld/measurements/curie01/'
measurements_dir = os.getcwd().replace('\\', '/') + '/measurements/repetitions/'
slurm_job_ids = [885086, 885088, 885089, 885090, 885091]
slurm_job_id_single_original = 884562
interesting_methods = [
    "kanzi.transform.ROLZCodec$ROLZEncoder.reset",
    "kanzi.entropy.ANSRangeEncoder.encode",
    "kanzi.entropy.CMPredictor.<init>",
    "kanzi.entropy.TPAQPredictor.<init>,",
    "kanzi.transform.FSDCodec.forward",
    "kanzi.io.CompressedOutputStream.write",
    "kanzi.entropy.BinaryEntropyEncoder.encodeByte",
    "kanzi.transform.ROLZCodec$ROLZCodec2.<init>",
    "kanzi.transform.ROLZCodec$ROLZEncoder.<init>",
    "kanzi.transform.Sequence.forward"]


def learn_repetion_models():
    all_models_e = {}
    all_models_t = {}
    for slurm_id in slurm_job_ids:
        config_measurements, models = learning.learn(measurements_dir + str(slurm_id) + '/')
        for method in models:
            method_name = method['method']
            if method_name not in all_models_e.keys():
                all_models_e[method_name] = []
                all_models_t[method_name] = []
            try:
                all_models_e[str(method_name)].append(method['energy_errors']['Mean Absolute Percentage'])
            except TypeError:
                print("TypeError@energy:", method_name, slurm_id)
            try:
                all_models_t[str(method_name)].append(method['performance_errors']['Mean Absolute Percentage'])
            except TypeError:
                print("TypeError@time:", method_name, slurm_id)
    print(all_models_e)
    return all_models_e, all_models_t


def plot_repetitions_high_energy_mape_methods():
    all_models_e, all_models_t = learn_repetion_models()
    x = np.array([], dtype=[('method_name', 'U300'), ('mape', 'f8')])
    for method in all_models_e.keys():
        # if method in interesting_methods:
        for mape in all_models_e[method]:
            x.resize(x.shape[0] + 1)
            x[-1] = (method, mape)
    df = pd.DataFrame(data=x)
    sns.set_theme(style="ticks")
    # Initialize the figure with a logarithmic x axis
    f, ax = plt.subplots(figsize=(13, 100))
    # ax.set_xscale("log")
    # Plot the orbital period with horizontal boxes
    sns.boxplot(x="mape", y="method_name", data=df, whis=[0, 100], width=.5, palette="vlag")
    # Add in points to show each observation
    sns.stripplot(x="mape", y="method_name", data=df,
                  size=4, color=".3", linewidth=0)

    # Tweak the visual presentation
    plt.xticks([0, 5, 10, 15, 25, 50, 100, 200, 300])
    ax.set(ylabel="Kanzi Method")
    ax.set(xlabel="MAPE (%)")
    sns.despine(trim=True, left=True)
    plt.savefig("figures/repeated_high_energy_mape_methods.png")
    # plt.show()


def t_e_qq_plot():
    t_e_mape_map = []
    for slurm_id in slurm_job_ids:
        config_measurements, models = learning.learn(measurements_dir + str(slurm_id) + '/')
        for method in models:
            method_name = method['method']
            try:
                mape_e = method['energy_errors']['Mean Absolute Percentage']
                mape_t = method['performance_errors']['Mean Absolute Percentage']
            except TypeError:
                print("TypeError@energy:", method_name, slurm_id)
                continue
            if method_name in interesting_methods:
                t_e_mape_map.append((slurm_id, method_name, mape_e, mape_t))
    e_t_mape_df = pd.DataFrame(data=t_e_mape_map,
                               columns=['repetition', 'method_name', 'Energy MAPE (%)', 'Time MAPE (%)'])
    e_t_mape_df.to_csv("et_mape.csv")
    """
    e_t_mape_df = pd.read_csv("et_mape.csv")
    """
    e_t_mape_plot = sns.relplot(
        data=e_t_mape_df, palette="Set2", kind="line", style="method_name", markers=True,
        y="Time MAPE (%)", x="Energy MAPE (%)", hue="method_name",  # hue="repetition",
    )
    # e_t_mape_plot.set_axis_labels("Snoot length (mm)", "Snoot depth (mm)")
    plt.suptitle("MAPE v. Energie- und Perf.-Modell aller Methoden")
    plt.savefig("figures/mape_e_t.png")
    plt.show()


def plot_variation():
    all_models_e = {}
    all_models_t = {}
    t_e_mape_map = []
    for slurm_id in slurm_job_ids:
        config_measurements, models = learning.learn(measurements_dir + str(slurm_id) + '/')
        for method in models:
            method_name = method['method']
            try:
                mape_e = method['energy_errors']['Mean Absolute Percentage']
                mape_t = method['performance_errors']['Mean Absolute Percentage']
            except TypeError:
                print("TypeError@energy:", method_name, slurm_id)
                continue
            t_e_mape_map.append((slurm_id, method_name, mape_e, mape_t))
    for slurm_id, method_name, mape_e, mape_t in t_e_mape_map:
        if method_name not in all_models_e.keys():
            all_models_e[method_name] = []
            all_models_t[method_name] = []
        try:
            all_models_e[str(method_name)].append(mape_e)
        except TypeError:
            print("TypeError@energy:", method_name, slurm_id)
        try:
            all_models_t[str(method_name)].append(mape_t)
        except TypeError:
            print("TypeError@energy:", method_name, slurm_id)
    covs_e = []
    for method in all_models_e.keys():
        cov = variation(all_models_e[method])
        covs_e.append((method, cov))
    covs_t = []
    for method in all_models_t.keys():
        cov = variation(all_models_t[method])
        covs_t.append((method, cov))
        # for energy
    e_cov_df = pd.DataFrame(data=covs_e, columns=['method_name', 'Variation of Energy MAPE'])
    sns.histplot(data=e_cov_df, x="Variation of Energy MAPE", bins=30)
    plt.savefig("figures/mape_variation_energy.png")
    plt.show()
    # for time not implemented for now


def read_energy_measurements():
    working_dir = 'D:/workbench/archiv_measure/measurements/curie01/'
    repetition_ids = list(
        {896371, 896379, 896380, 896381, 896382, 896383, 885090, 898331, 898332, 898333, 898334, 898335, 898336, 898337,
         898338, 898339, 902067, 902069, 902070, 885091})
    print(len(repetition_ids), "measurement runs")
    repetitions = []
    config_dict = {}
    parse_index = 0
    for repetition_id in repetition_ids:
        parse_index += 1
        print("Start parsing Repetition #" + str(parse_index))
        rep_measurements = modeling_target_system.parse_config_measurements(working_dir + str(repetition_id) + '/')
        repetitions.append(rep_measurements)
    parse_index = 0
    for repetition in repetitions:
        parse_index += 1
        print("Start adding Repetition #" + str(parse_index))
        for config in repetition:
            config_name = str(config)
            if config_name not in config_dict.keys():
                config_dict[config_name] = {}
            for index, row in config.energy.iterrows():
                method_name = row['method']
                if method_name not in config_dict[config_name].keys():
                    config_dict[config_name][method_name] = []
                config_dict[config_name][method_name].append(row['e'])
    print("#END#config_dict:")
    print(config_dict)
    return config_dict


def analyse_energy_repetitions():
    # config_dict = read_energy_measurements()
    file = open("config_dict.txt", "r")
    contents = file.read()
    config_dict = ast.literal_eval(contents)
    file.close()
    print(type(config_dict), "object")
    print(len(config_dict), "configs")

    for config in config_dict:
        if 'kanzi.entropy.BinaryEntropyEncoder.encodeByte' in config_dict[config]:
            # print(variation(config_dict[config]['kanzi.entropy.BinaryEntropyEncoder.encodeByte']))
            a = 0
        if 'kanzi.entropy.NullEntropyEncoder.encode' in config_dict[config]:
            print(variation(config_dict[config]['kanzi.entropy.NullEntropyEncoder.encode']))


if __name__ == '__main__':
    # Configure working directory first!
    # plot_repetitions_high_energy_mape_methods()
    # t_e_qq_plot()
    # plot_variation()
    # read_energy_measurements()
    analyse_energy_repetitions()
