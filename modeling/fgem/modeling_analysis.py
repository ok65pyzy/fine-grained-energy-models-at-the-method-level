import numpy as np

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def analyse_results(models):
    e_mape = []
    t_mape = []
    e_intercept_mape = []
    t_intercept_mape = []

    for model in models:
        if model['energy_errors'] != 0 and model['energy_errors']['Mean Absolute Percentage'] != -1:
            e_mape.append(model['energy_errors']['Mean Absolute Percentage'])
            e_intercept_mape.append((model['energy_model'].named_steps['clf'].intercept_ / 1000,
                                     model['energy_errors']['Mean Absolute Percentage']))
        if model['performance_errors'] != 0 and model['performance_errors']['Mean Absolute Percentage'] != -1:
            t_mape.append(model['performance_errors']['Mean Absolute Percentage'])
            t_intercept_mape.append((model['performance_model'].named_steps['clf'].intercept_ / 1000,
                                     model['performance_errors']['Mean Absolute Percentage']))
    print("e_mape:")
    print(e_mape)
    print("t_mape:")
    print(t_mape)
    df = pd.DataFrame(e_mape, columns=['MAPE (%)'])
    mape_hist = sns.histplot(df, bins=100)
    mape_hist.set(ylabel='Count (#)')
    mape_hist.set_title("Verteilung des Fehlers für jede Methode")
    plt.savefig("figures/error_histogramm_e.png")
    # print(df.describe())

    t_intercept_mape_df = pd.DataFrame(data=t_intercept_mape, columns=['intercept (s)', 'MAPE (%)'])

    e_intercept_mape_df = pd.DataFrame(data=e_intercept_mape, columns=['intercept (Ws)', 'MAPE (%)'])
    e_intercept_mape_plot = sns.jointplot(y="MAPE (%)", x="intercept (Ws)", data=e_intercept_mape_df,
                                          kind="scatter", height=7)
    e_intercept_mape_plot.ax_joint.set_xscale('log')
    plt.suptitle("Energie-Minimum und Fehler je Methode")
    plt.savefig("figures/intercept_mape_e.png")
    # plt.show()
    return e_intercept_mape_df, t_intercept_mape_df


def analyse_outliers(config_measurements, models):
    # calculate share method-energy/total-energy
    method_accu_energy = {'total': 0.0}
    method_accu_time = {'total': 0.0}
    for config in config_measurements:
        method_accu_energy['total'] += np.sum(config.energy['e'])
        method_accu_time['total'] += np.sum(config.time['t'])
        for index, row in config.energy.iterrows():
            method_name = row['method']
            if method_name not in method_accu_energy.keys():
                method_accu_energy[method_name] = 0.0
            method_accu_energy[method_name] += row['e']
            method_accu_time[method_name] += row['t']

    e_big_error = []
    # relate method mape with method share
    for model in models:
        if model['energy_errors'] != 0 and model['energy_errors']['Mean Absolute Percentage'] != -1:
            # if model['energy_errors']['Mean Absolute Percentage'] > 10:
            e_big_error.append((model['method'],
                                model['energy_errors']['Mean Absolute Percentage']))
    mapes = pd.DataFrame(e_big_error, columns=['method', 'MAPE (%)']).sort_values(by=['MAPE (%)'], ascending=False)

    mape_share_rel = []
    for index, row in mapes.iterrows():
        # mape = float(f"{row['MAPE (%)']:1.2f}")
        # share = float(f"{method_accu_energy[row['method']] / method_accu_energy['total'] * 100.0:3.1f}")
        mape = row['MAPE (%)']
        share = method_accu_energy[row['method']] / method_accu_energy['total'] * 100.0
        method_name = row['method']
        mape_share_rel.append((mape, share, method_name))
    share_mape_df = pd.DataFrame(data=mape_share_rel, columns=['MAPE (%)', 'Energy share (%)', 'Method'])
    print("")
    print(share_mape_df.sort_values(by=['Energy share (%)'], ascending=False).head(20))
    # > 0.1% share
    share_mape_plot = sns.jointplot(y="MAPE (%)", x="Energy share (%)", data=share_mape_df, kind="scatter", height=8.5,
                                    xlim=(0.1, 26), ylim=(0, 40))
    share_mape_plot.ax_joint.set_xscale('log')
    plt.suptitle("Energie-Anteil (ab 0.1%) und Fehler je Methode ")
    plt.savefig("figures/share_mape_e_share_more_01_promill.png")
    # all
    share_mape_plot = sns.jointplot(y="MAPE (%)", x="Energy share (%)", data=share_mape_df, kind="scatter", height=8.5)
    share_mape_plot.ax_joint.set_xscale('log')
    plt.suptitle("Energie-Anteil und Fehler je Methode")
    plt.savefig("figures/share_mape_e.png")

    # plt.show()
    print("\ntotal\t\t\t\t\t\t", len(share_mape_df))
    print("MAPE > 10%\t\t\t\t\t", len(share_mape_df[share_mape_df['MAPE (%)'] > 10]))
    print("MAPE > 10% and SHARE >= .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] > 10) & (share_mape_df['Energy share (%)'] >= 0.1)]))
    print("MAPE > 10% and SHARE < .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] > 10) & (share_mape_df['Energy share (%)'] < 0.1)]))
    print("MAPE > 5%\t\t\t\t\t", len(share_mape_df[share_mape_df['MAPE (%)'] > 5]))
    print("MAPE > 5% and SHARE >= .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] > 5) & (share_mape_df['Energy share (%)'] >= 0.1)]))
    print("MAPE > 5% and SHARE < .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] > 5) & (share_mape_df['Energy share (%)'] < 0.1)]))
    print("MAPE <= 5% and SHARE > .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] <= 5) & (share_mape_df['Energy share (%)'] > 0.1)]))
    print("MAPE <= 5% and SHARE <= .1%\t",
          len(share_mape_df[(share_mape_df['MAPE (%)'] <= 5) & (share_mape_df['Energy share (%)'] <= 0.1)]))
    print("\nMAPE > 10% and SHARE > .1%:")
    print(share_mape_df[(share_mape_df['MAPE (%)'] > 10) & (share_mape_df['Energy share (%)'] >= 0.1)])


def print_models(models) -> None:
    for method in models:
        print("method:      ", method['method'])
        if (method['energy_model'] != 0):
            print('\tenergy:     ', end='')
            for ec in method['energy_model'].named_steps['clf'].coef_:
                print(f'{ec:5.0f}', end='')
            print("")
            print("\te-intercept:", f"{method['energy_model'].named_steps['clf'].intercept_:5.0f}")
            print('\te-errors:     ', end='')
            for er in method['energy_errors'].keys():
                if method['energy_errors'][er] > -1:
                    print(er, end=':')
                    print(f"{method['energy_errors'][er]:10.2f}", end=', ')
            print("")
        else:
            print("no energy data")
        print("")
        if method['performance_model'] != 0:
            print('\ttime:       ', end='')
            for tc in method['performance_model'].named_steps['clf'].coef_:
                print(f'{tc:5.0f}', end='')
            print("")
            print("\tt-intercept:", f"{method['performance_model'].named_steps['clf'].intercept_:5.0f}")
            print('\tt-errors:     ', end='')
            for et in method['performance_errors'].keys():
                if method['performance_errors'][et] > -1:
                    print(et, end=':')
                    print(f"{method['performance_errors'][et]:10.2f}", end=', ')
        else:
            print("no time data")
        print("\n\n")
