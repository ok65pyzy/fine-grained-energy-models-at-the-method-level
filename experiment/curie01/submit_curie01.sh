#!/bin/bash

NTASKS=140
PARTITION='curie'
JOB_NAME='finegrained'
NODELIST='curie01'

sbatch --exclusive --array=1-$NTASKS --partition=$PARTITION --nodelist=$NODELIST --job-name="${JOB_NAME}" slurm_bench_curie.sh
