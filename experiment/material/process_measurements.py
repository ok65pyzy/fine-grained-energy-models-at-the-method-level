import numpy as np
import pandas as pd

import os
import sys


def parse_energy_data(job):
    directory_name = job['cwd']
    dir_list = os.listdir(directory_name + "/energy/")
    energy_file = directory_name + "/energy/" + dir_list[0] + "/energy_measurements.csv"
    processed_csv_file = directory_name + "/energy/" + dir_list[0] + "/energy_out_"

    df = pd.read_csv(energy_file, sep=',')

    map_bin_cpu_1 = []
    map_bin_cpu_2 = []

    for index, row in df.iterrows():
        energy_time_map = np.zeros((799,), dtype=[('unix_time_ms', 'f8'), ('e', 'f8')])
        address = row[1]
        vals = row[2].split()
        unix_timestamp_nano = int(parse_number(vals[1]) * 10 ** 9)
        if "CPU_" in address:
            string_array = np.array(vals[2:801])
            i = 0
            for val in string_array:
                energy_time_map[i] = (
                    # float((unix_timestamp_nano + i * measurement_time_span) / 10**6), parse_number(val))
                    float((unix_timestamp_nano) / 10 ** 6), parse_number(val))
                i += 1
            if "CPU_1" in address:
                map_bin_cpu_1.append(energy_time_map)
            elif "CPU_2" in address:
                map_bin_cpu_2.append(energy_time_map)

    final_energy_time_map_cpu_1 = calculate_timestamps(map_bin_cpu_1)
    final_energy_time_map_cpu_2 = calculate_timestamps(map_bin_cpu_2)
    print(len(final_energy_time_map_cpu_1), '+', len(final_energy_time_map_cpu_2), "energy measurement points")
    e_t_map_cpu_1 = pd.DataFrame(data=final_energy_time_map_cpu_1)
    e_t_map_cpu_2 = pd.DataFrame(data=final_energy_time_map_cpu_2)
    e_t_map_cpu_1.to_csv(processed_csv_file + "cpu1.csv", index=False, sep=';')
    e_t_map_cpu_2.to_csv(processed_csv_file + "cpu2.csv", index=False, sep=';')

    return e_t_map_cpu_1, e_t_map_cpu_2


def calculate_timestamps(map_bin):
    final_energy_time_map = np.array([], dtype=[('unix_time_ms', 'f8'), ('e', 'f8')])
    map_bin_index = 0
    for e_t_map in map_bin:
        energy_time_map = np.zeros((799,), dtype=[('unix_time_ms', 'f8'), ('e', 'f8')])
        current_unix = e_t_map[0]['unix_time_ms']
        if len(map_bin) > map_bin_index + 1:
            current_gap = float((map_bin[map_bin_index + 1][0]['unix_time_ms'] - current_unix) / 798)
        current_count = 0
        for row in e_t_map:
            energy_time_map[current_count] = (current_unix + current_count * current_gap, row['e'])
            current_count += 1
        final_energy_time_map = np.concatenate((final_energy_time_map, energy_time_map), axis=0)
        map_bin_index += 1
    return final_energy_time_map


def parse_number(raw: str) -> float:
    return float(raw.replace('[', '').replace(']', '').replace(',', ''))


def parse_traces(job):
    directory_name = job['cwd']
    traces_file = directory_name + "/traces/traces.log"
    filtered_file_name = directory_name + "/traces/filtered.csv"
    traces_csv_file = directory_name + "/traces/traces.csv"

    file = open(traces_file, "r")
    list_of_lines = file.readlines()
    file.close()
    kanzi_l = ''
    kanzi_b = ''
    start_ts = 0.0
    end_ts = 0.0

    i = 0
    for line in list_of_lines:
        if "currentTimeMillis=" in line:
            if start_ts == 0:
                start_ts = float(line.split("currentTimeMillis=")[1])
            else:
                end_ts = float(line.split("currentTimeMillis=")[1])
            list_of_lines[i] = ""
        elif "slurm_job" in line:
            list_of_lines[i] = ""
            kanzi_l = line.split("l_")[1].split("_")[0]
            kanzi_b = line.split("b_")[1]
        elif "$trace" not in line and line != "":
            list_of_lines[i] = ""
        i += 1

    list_of_lines[0] = '"marker";"rel_time";"method"\n'
    file = open(filtered_file_name, "w")
    file.writelines(list_of_lines)
    file.close()

    df = pd.read_csv(filtered_file_name, sep=';', usecols=['rel_time', 'method'])
    print('Dimension of method trace dataset is', df.shape)
    x = np.array([], dtype=[('unix_time_ms', 'f8'), ('method', 'U300')])
    t_0 = df.iloc[0]["rel_time"]

    print("process traces...", end='')
    blocked: float = -1.0
    for index, row in df.iterrows():
        if 'app//' in row['method'] and not blocked == row['rel_time']:
            method = row['method'].split('app//')[1].split('(')[0]
            t_in_ms = float(f'{(row["rel_time"] - t_0) * 0.000001:10.4f}')
            x.resize(x.shape[0] + 1)
            x[-1] = (start_ts + t_in_ms, method)
            blocked = row['rel_time']
        if index % int(i / 20) == 0:
            print(f'{index / df.shape[0] * 100:3.0f}%', end='...')
    print("100%")
    print("kanzi took", int((blocked - t_0) / 10 ** 6), "ms")
    traces = pd.DataFrame(data=x)
    traces.to_csv(traces_csv_file, index=False, sep=';')
    return traces


def calc_method_energy(cpu1, cpu2, methods) -> dict:
    _t = 'unix_time_ms'
    _e = 'e'
    begin_unix = methods.iloc[0][_t]
    end_unix = methods.iloc[-1][_t]
    kanzi_time = end_unix - begin_unix
    ffem_ten_seconds: float = cpu1.iloc[0][_t] + 10 ** 4
    # energy threshold for determing when the program starts
    threshold = 6
    begin_ffem = cpu1[(cpu1[_t] > ffem_ten_seconds) & (cpu1[_e] > threshold)].iloc[0][_t]
    print("kanzi took", int(end_unix - begin_unix), "ms")
    print("ffem  took", int(cpu1.iloc[-1][_t] - cpu1.iloc[0][_t]), "ms")
    cut_cpu1 = cpu1[(cpu1[_t] >= begin_ffem) & (cpu1[_t] <= begin_ffem + kanzi_time)]
    offset = begin_ffem - begin_unix
    print('offset is', int(offset), 'ms')
    method_energy = {}
    for index, row in methods.iterrows():
        method_name = row['method']
        if method_name not in method_energy.keys():
            method_energy[method_name] = 0.0
        t = row[_t]
        if index + 1 < methods.shape[0]:
            t_next = methods.iloc[index + 1][_t]
            energy_cpu1 = cpu1[(cpu1[_t] >= t + offset) & (cpu1[_t] <= t_next + offset)]
            energy_cpu2 = cpu2[(cpu2[_t] >= t + offset) & (cpu2[_t] <= t_next + offset)]
            method_energy[method_name] += calc_aggregated_energy(energy_cpu1, t, t_next, offset)
            method_energy[method_name] += calc_aggregated_energy(energy_cpu2, t, t_next, offset)
    return sorted(method_energy.items(), key=lambda x: x[1], reverse=True)


def calc_aggregated_energy(energy_points_inside, t: float, t_next: float, offset: float) -> float:
    _t = 'unix_time_ms'
    _e = 'e'
    e: float = []
    if len(energy_points_inside) > 0:
        start_gap = energy_points_inside.iloc[0][_t] - offset - t
        e.append(start_gap * energy_points_inside.iloc[0][_e])
        end_gap = t_next - (energy_points_inside.iloc[-1][_t] - offset)
        e.append(end_gap * energy_points_inside.iloc[-1][_e])
        help_i = 0
        for e_i, e_r in energy_points_inside.iterrows():
            if help_i + 1 < energy_points_inside.shape[0]:
                e.append(e_r[_e] * (energy_points_inside.iloc[help_i + 1][_t] - e_r[_t]))
            help_i += 1
    return sum(e)


def calc_method_times(methods) -> dict:
    _t = 'unix_time_ms'
    method_times = {}
    for index, row in methods.iterrows():
        method_name = row['method']
        if method_name not in method_times.keys():
            method_times[method_name] = 0.0
        t = row[_t]
        if index + 1 < methods.shape[0]:
            t_next = methods.iloc[index + 1][_t]
            method_times[method_name] += (t_next - t)
    return sorted(method_times.items(), key=lambda x: x[1], reverse=True)


def write_result_csv(energy, times, job):
    energy_file = job['cwd'] + "/energy/energy_result.csv"
    times_file = job['cwd'] + "/traces/times_result.csv"
    pd.DataFrame.from_dict(energy).to_csv(energy_file, index=False, sep=';')
    with open(energy_file) as f:
        lines = f.readlines()
    lines[0] = "method;e\n"
    with open(energy_file, "w") as f:
        f.writelines(lines)
    pd.DataFrame.from_dict(times).to_csv(times_file, index=False, sep=';')
    with open(times_file) as f:
        lines = f.readlines()
    lines[0] = "method;t\n"
    with open(times_file, "w") as f:
        f.writelines(lines)


if __name__ == '__main__':
    # SET YOUR LINUX USER NAME HERE:
    cwd = '/tmp/kronfeld/measurements'
    slurm_job = {
        'cwd': cwd
    }
    t_e_map_cpu_1, t_e_map_cpu_2 = parse_energy_data(slurm_job)
    t_method_map = parse_traces(slurm_job)

    method_energy_dict = calc_method_energy(t_e_map_cpu_1, t_e_map_cpu_2, t_method_map)
    print(method_energy_dict)
    method_times_dict = calc_method_times(t_method_map)
    print(method_times_dict)
    write_result_csv(method_energy_dict, method_times_dict, slurm_job)

