import sys
import time
import subprocess
import swsEnergyMetering.mqttEnergy as metering


@metering.energy_metering
def work(args):
    time.sleep(2)
    out = subprocess.Popen(args[1:])
    # res = out.communicate()
    time.sleep(2)
    # return res


def wrapper(args):
    start = time.time()
    work(args)
    end = time.time()
    # if output is not None:
    #    print('BASH o:', str(output, 'UTF-8'))
    # if error is not None:
    #    print('BASH e:', str(error, 'UTF-8'))


if __name__ == "__main__":
    wrapper(sys.argv)
