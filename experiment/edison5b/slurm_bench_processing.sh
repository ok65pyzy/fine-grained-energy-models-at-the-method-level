#!/bin/bash
#
#SBATCH --output=/home/kronfeld/logs/slurm_log_%x-%j.out
#SBATCH --error=/home/kronfeld/logs/slurm_log_%x-%j.err

hostname
date

MEASUREMENT_JOB_ID=$1
MEASUREMENT_ARRAY_TASK_ID=$2

# prepare filesystem
rm -rf /tmp/kronfeld/*
mkdir -p /tmp/kronfeld/experiment
mkdir -p /tmp/kronfeld/measurements
mkdir -p /home/kronfeld/measurements/curie01/${MEASUREMENT_JOB_ID}/${MEASUREMENT_ARRAY_TASK_ID}/results
rm -rf /home/kronfeld/measurements/curie01/${MEASUREMENT_JOB_ID}/${MEASUREMENT_ARRAY_TASK_ID}/results/*

# copy measurement data from network storage to local storage
cp /home/kronfeld/slurm/material/process_measurements.py /tmp/kronfeld/experiment/process_measurements.py
cp -rv /home/kronfeld/measurements/curie01/${MEASUREMENT_JOB_ID}/${MEASUREMENT_ARRAY_TASK_ID}/* /tmp/kronfeld/measurements/

# run
echo "run #${MEASUREMENT_JOB_ID}-${MEASUREMENT_ARRAY_TASK_ID}"
python3 /tmp/kronfeld/experiment/process_measurements.py ${MEASUREMENT_JOB_ID} ${MEASUREMENT_ARRAY_TASK_ID}

echo 'copy results'
# copy results from tmp directory to home
cp /tmp/kronfeld/measurements/energy/energy_result.csv /home/kronfeld/measurements/curie01/${MEASUREMENT_JOB_ID}/${MEASUREMENT_ARRAY_TASK_ID}/results/energy_result.csv
cp /tmp/kronfeld/measurements/traces/times_result.csv /home/kronfeld/measurements/curie01/${MEASUREMENT_JOB_ID}/${MEASUREMENT_ARRAY_TASK_ID}/results/times_result.csv
echo 'done'
# end

scancel ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}
