#!/bin/bash
#
#SBATCH --output=/home/kronfeld/logs/slurm_log_%x-%j.out
#SBATCH --error=/home/kronfeld/logs/slurm_log_%x-%j.err

hostname
date

# prepare filesystem
rm -rf /tmp/kronfeld/*
mkdir -p /tmp/kronfeld/energy
mkdir -p /tmp/kronfeld/environments
mkdir -p /tmp/kronfeld/performance
mkdir -p /tmp/kronfeld/experiment
mkdir -p /tmp/kronfeld/traces

# create venv
python3 -m virtualenv /tmp/$USER/environments/energy_env
source /tmp/$USER/environments/energy_env/bin/activate


# install energy measurement scripts
python3 -m pip install /home/kronfeld/energy-measurement/cpu_benchmark_test/energy-metering/ --no-cache-dir

# copy experiment data from network storage to local storage
cp -rv /home/kronfeld/slurm/material /tmp/kronfeld/experiment/
cp -rv /home/kronfeld/curie01/exec_bash.py /tmp/kronfeld/experiment/exec_bash.py

config_file='/tmp/kronfeld/experiment/material/konfigs.txt'
config_length=`wc -l ${config_file} | sed -r 's/[^0-9]//g'`
config_line=`sed -n ${SLURM_ARRAY_TASK_ID}p ${config_file}`
config_parameters=`echo ${config_line} | sed -r 's/(".*"|prefix|postfix|\s\s)//g'`

if [[ ${SLURM_ARRAY_TASK_ID} -le ${config_length} ]] && [[ ${SLURM_ARRAY_TASK_ID} -ge 1 ]]
then
    # run
    touch /tmp/kronfeld/traces/traces.log
    echo ${config_parameters} > /tmp/kronfeld/traces/traces.log
    echo "run #${SLURM_ARRAY_TASK_ID} --- ${config_parameters}"
    python3 /tmp/kronfeld/experiment/exec_bash.py java -javaagent:/tmp/kronfeld/experiment/material/agent-1.1.jar -cp /tmp/kronfeld/experiment/material/kanzi-1.9.0.jar kanzi.app.Kanzi -c -f -i /tmp/kronfeld/experiment/material/sample_file_20MB ${config_parameters} >> /tmp/kronfeld/traces/traces.log
    # python3 /tmp/kronfeld/experiment/exec_bash.py java -cp /tmp/kronfeld/experiment/material/kanzi-1.9.0.jar kanzi.app.Kanzi -c -f -i /tmp/kronfeld/experiment/material/sample_file_20MB ${config_parameters} >> /tmp/kronfeld/traces/traces.log
else
  echo "Task ID is not present in lines of configuration file!"
fi

#./run_benchmark.sh

echo 'copy results'
ls /tmp/kronfeld/energy
ls /tmp/kronfeld/performance
# copy results from tmp directory to home
mkdir -p /home/kronfeld/measurements/$HOSTNAME/${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}
cp -r /tmp/kronfeld/energy /home/kronfeld/measurements/$HOSTNAME/${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}
cp -r /tmp/kronfeld/performance /home/kronfeld/measurements/$HOSTNAME/${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}
cp -r /tmp/kronfeld/traces /home/kronfeld/measurements/$HOSTNAME/${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}

# deactivate venv
deactivate

scancel ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}
