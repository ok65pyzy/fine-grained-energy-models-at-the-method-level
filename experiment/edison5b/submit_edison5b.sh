#!/bin/bash

NTASKS=2
PARTITION='edison5b'
JOB_NAME='prcssmsrmnts'
NODELIST='edison[38,40-49,51-56]'

if [ -z "$1" ] || [ -z "$2" ]
then
    echo 'usage: ./submit_edison5b.sh <measurement_slurm_job_id> <measurement_slurm_array_task_id>'
else
    sbatch --exclusive --partition=$PARTITION --nodelist=$NODELIST --job-name="${JOB_NAME}" slurm_bench_processing.sh $1 $2
fi