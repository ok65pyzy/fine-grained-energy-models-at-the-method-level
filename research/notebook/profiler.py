import numpy as np
import matplotlib.pyplot as plt


class ProfilerGraph:
    def __init__(self, data, labels):
        assert len(data) == len(labels), "label datasets must have same length as time dataset"
        data_cum = np.array(data).cumsum()
        method_colors = plt.get_cmap('RdYlGn')(
            np.linspace(0.15, 0.85, np.array(data).shape[0]))

        fig, ax = plt.subplots(figsize=(10.0, 2))
        ax.set_xlim(0, data[len(data)-1])

        for i, (colname, color) in enumerate(zip(labels, method_colors)):
            widths = data[i] - data[i - 1]
            if i == 0:
                widths = data[i]
            starts = data_cum[i] - data_cum[i - 1] - widths
            if i == 0:
                starts = data_cum[i] - widths
            ax.set(ylabel='Profiler', xlabel='time (s)')
            rects = ax.barh([''], widths, left=starts, height=0.5,
                            label=colname, color=color)

            r, g, b, _ = color
            text_color = 'black'
            ax.bar_label(rects, label_type='center', color=text_color)
        ax.legend(ncol=len(labels), bbox_to_anchor=(0, 1),
                  loc='lower left', fontsize='small')
        plt.show()
