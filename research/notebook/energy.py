import numpy as np
import matplotlib.pyplot as plt


class EnergyGraph:
    def __init__(self, values):
        t = np.arange(0.0, len(values), 1.0)
        fig, ax = plt.subplots(figsize=(8.0, 2))
        ax.plot(t, values)
        ax.set(xlabel='time (s)', ylabel='current (mA)')
        ax.grid()
        plt.show()


class EPSyncedGraph:
    def __init__(self, e_values, p_times, p_labels):
        assert len(p_times) == len(p_labels), "Label datasets must have same length as profiler time dataset"
        assert len(e_values) - 1 == p_times[-1], "Profiler and energy datasets must be same time length"
        data_cum = np.array(p_times).cumsum()
        t = np.arange(0.0, len(e_values), 1.0)
        fig, (ax2, ax1) = plt.subplots(2, sharex=True, figsize=(10.0, 5))
        ax1.plot(t, e_values)
        ax1.set(ylabel='current (mA)', xlabel='time (s)')
        ax1.grid()
        method_colors = plt.get_cmap('RdYlGn')(
            np.linspace(0.15, 0.85, np.array(p_times).shape[0]))
        for i, (colname, color) in enumerate(zip(p_labels, method_colors)):
            widths = p_times[i] - p_times[i - 1]
            if i == 0:
                widths = p_times[i]
            starts = data_cum[i] - data_cum[i - 1] - widths
            if i == 0:
                starts = data_cum[i] - widths
            ax2.set(ylabel='Profiler')
            rects = ax2.barh([''], widths, left=starts, height=0.5,
                             label=colname, color=color)

            r, g, b, _ = color
            text_color = 'black'
            ax2.bar_label(rects, label_type='center', color=text_color)
        plt.show()
