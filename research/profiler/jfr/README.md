## Method profiling using Java Flight Recorder

The examples are for Java 11 (e.g. OpenJDK), older versions are slightly different

#### Use JFR with

```
java -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -XX:StartFlightRecording=settings=custom.jfc,filename=methods.jfr -cp <YOUR_CLASS>
```

e.g.

```
java -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -XX:StartFlightRecording=settings=custom.jfc,filename=methods.jfr -cp kanzi-1.9.0.jar kanzi.app.Kanzi -c -f -i sample_file_10MB -l 6 -b 1024
```

#### Export execution traces

```
jfr print methods.jfr > execution-traces.txt
```

#### Settings file 

`custom.jfc`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration label="ContinuousTraces">
    <event name="jdk.ExecutionSample">
      <setting name="enabled">true</setting>
      <setting name="period">10 ms</setting>
    </event>
</configuration>
```

#### Example output

`execution-traces.txt`

```yaml
jdk.ExecutionSample {
  startTime = 16:38:17.161
  sampledThread = "main" (javaThreadId = 1)
  state = "STATE_RUNNABLE"
  stackTrace = [
    java.util.zip.InflaterInputStream.read(byte[], int, int) line: 159
    jdk.internal.loader.Resource.getBytes() line: 124
    jdk.internal.loader.URLClassPath$JarLoader$2.getBytes() line: 882
    jdk.internal.loader.BuiltinClassLoader.defineClass(String, Resource) line: 797
    jdk.internal.loader.BuiltinClassLoader.findClassOnClassPathOrNull(String) line: 698
    ...
  ]
}

jdk.ExecutionSample {
  startTime = 16:38:17.183
  sampledThread = "main" (javaThreadId = 1)
  state = "STATE_RUNNABLE"
  stackTrace = [
    kanzi.entropy.FPAQEncoder.encodeBit(int, int) line: 120
    kanzi.entropy.FPAQEncoder.encode(byte[], int, int) line: 93
    kanzi.io.CompressedOutputStream$EncodingTask.encodeBlock(SliceByteArray, SliceByteArray, int, long, int, int) line: 706
    kanzi.io.CompressedOutputStream$EncodingTask.call() line: 545
    kanzi.io.CompressedOutputStream$EncodingTask.call() line: 507
    ...
  ]
}

jdk.ExecutionSample {
  startTime = 16:38:17.196
  sampledThread = "main" (javaThreadId = 1)
  state = "STATE_RUNNABLE"
  stackTrace = [
    java.util.Arrays.fill(int[], int) line: 3389
    kanzi.entropy.FPAQEncoder.<init>(OutputBitStream) line: 61
    kanzi.entropy.EntropyCodecFactory.newEncoder(OutputBitStream, Map, int) line: 107
    kanzi.io.CompressedOutputStream$EncodingTask.encodeBlock(SliceByteArray, SliceByteArray, int, long, int, int) line: 703
    kanzi.io.CompressedOutputStream$EncodingTask.call() line: 545
    ...
  ]
}
```

