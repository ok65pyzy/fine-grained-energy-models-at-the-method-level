## Using Combined Java Profiler  (simple_agent) to create method histograms

Directory should contain the following, `out/` and `traces/` need to be empty

```
agent-1.0-all.jar
kanzi-1.9.0.jar
out/
traces/
sample_file_100KB
sample_file_10MB
sample_file_1MB
process_traces.py
README.md
```

#### Slurm

```
process_traces.py -l <compress_level> -b <block_size> -s file_size <100KB|1MB|10MB>
```

#### Single Computer

```
see commented code
```
