#!/bin/python3

import multiprocessing
import subprocess
import os

cwd = os.getcwd()

# Number of cores goes here
CPU_CORES = 4


def worker(job):
    global cwd
    folder = "l_" + str(job['l']) + "_b_" + str(job['b'])
    bash_cmd = ["mkdir", folder, "&&", "cd", folder, "&&", "java", "-javaagent:" + cwd + "/agent-1.0-all.jar", "-classpath", cwd + "/kanzi-1.9.0.jar",
                "kanzi.app.Kanzi", "-c", "-i", cwd + "/sample_file_100KB", "-f", "-l", str(job['l']), "-b", str(job['b'])]
    return subprocess.call(bash_cmd)


def main():
    global CPU_CORES
    global cwd

    tasks = []

    # Features go here
    for l in range(9):
        for b in range(1024, 40960 + 1, 512):
            tasks.append({'b': b, 'l': l})

    pool = multiprocessing.Pool(processes=CPU_CORES)
    print("Did", str(len(tasks)), " profiling tasks")
    r = pool.map_async(worker, tasks)
    r.wait()


if __name__ == '__main__':
    main()
