import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import multiprocessing
import sys
import getopt
import subprocess
from subprocess import PIPE
import os
cwd = os.getcwd().replace('\\', '/')


def create_histogramm(job):
    directory_name = "traces/l_" + job['l'] + "_b_" + job['b'] + "_s_" + job['s']
    file_name = directory_name + '/method_trace.log'
    timestamp_file = directory_name + '/ts.log'

    file = open(file_name, "r")
    list_of_lines = file.readlines()
    file.close()

    if "currentTimeMillis=" in list_of_lines[0]:
        start_ts = float(list_of_lines[0].split("currentTimeMillis=")[1])
        ts_file = open(timestamp_file, "a")
        ts_file.write("start=" + str(start_ts) + "\n")
        ts_file.close()
        list_of_lines[0] = '"method";"start";"end"\n'
    else:
        ts_file = open(timestamp_file, "r")
        start_ts = float(ts_file.readlines()[0].split("start=")[1])
        ts_file.close()
    if "currentTimeMillis=" in list_of_lines[-1]:
        end_ts = float(list_of_lines[-1].split("currentTimeMillis=")[1])
        ts_file = open(timestamp_file, "a")
        ts_file.write("end=" + str(end_ts) + "\n")
        ts_file.close()
        list_of_lines[-1] = ""
    else:
        ts_file = open(timestamp_file, "r")
        end_ts = float(ts_file.readlines()[1].split("end=")[1])
        ts_file.close()

    file = open(file_name, "w")
    file.writelines(list_of_lines)
    file.close()

    df = pd.read_csv(file_name, sep=';')

    print('Dimension of dataset is ', df.shape)
    print("Absolute time span is", start_ts, "-", end_ts)
    x = np.array([], dtype=[('mod', 'i8'), ('count', 'i8')])
    for index, row in df.iterrows():
        mod = row["start"] % 10000
        tp_arr = [item for item in x if item[0] == mod]
        if len(tp_arr) == 0:
            x.resize(x.shape[0] + 1)
            x[-1] = (mod, 1)
        else:
            [item for item in x if item[0] == mod][0][1] = tp_arr[0][1] + 1
        if index % 10000 == 0:
            print(f'{index / df.shape[0] * 100:3.1f}', "%")
    print("100 %")

    left, width = 0.1, 0.8
    bottom, height = 0.1, 0.65
    rect_scatter = [left, bottom, width, height]
    fig = plt.figure(figsize=(7, 3))
    ax = fig.add_axes(rect_scatter)
    #ax.set_xscale('log')
    #ax.set_yscale('log')
    ax.scatter(x['mod'], x['count'])

    plt.show()
    #plt.savefig("out/histogram_l_" + job['l'] + "_b_" + job['b'] + "_s_" + job['s'] + ".png")


def start_profiling(job):
    global cwd
    folder = "l_" + str(job['l']) + "_b_" + str(job['b']) + "_s_" + str(job['s'])
    if job['profiler']:
        bash_cmd = ["java", "-classpath", cwd + "/kanzi-1.9.0.jar",
                    "kanzi.app.Kanzi", "-c", "-i", cwd + "/sample_file_" + job['s'], "-f", "-l", str(job['l']), "-b", str(job['b'])]
    else:
        bash_cmd = ["cd", "traces", "&&", "mkdir", folder, "&&", "cd", folder, "&&", "java", "-javaagent:" + cwd + "/agent-1.0-all-main.jar", "-classpath", cwd + "/kanzi-1.9.0.jar",
                "kanzi.app.Kanzi", "-c", "-i", cwd + "/sample_file_" + job['s'], "-f", "-l", str(job['l']), "-b", str(job['b'])]

    child = subprocess.Popen(bash_cmd, shell=True, stdout = PIPE, stderr = PIPE)
    exit_code = child.wait()
    if exit_code == 0:
        out_arr = child.communicate()[0].split()
        for i in range(len(out_arr)):
            if str(out_arr[i]) == 'b\'ms\'':
                took = str(out_arr[i-1]).split('\'')[1]
                print("took", took, "ms")
                tt_file = open("total_times.log", "a")
                tt_file.write(str(took) + ";" + str(job['profiler']) + ";" + str(job['l']) + ";" + str(job['b']) + ";" + str(job['s']) + "\n")
                tt_file.close()
        #create_histogramm(job)
    else:
        streamdata = child.communicate()
        print(streamdata)


def main(argv):
    help = 'process_traces.py -l <compress_level> -b <block_size> -s file_size <100KB|1MB|10MB>'
    job = {'l': '', 'b': '', 's': ''}
    try:
        opts, args = getopt.getopt(argv,"hl:b:s:", ['compress_level=', 'block_size=', 'file_size=',])
    except getopt.GetoptError:
        print (help)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print (help)
            sys.exit()
        elif opt in ("-l", "--compress_level"):
            job['l'] = arg
        elif opt in ("-b", "--block_size"):
            job['b'] = arg
        elif opt in ("-s", "--file_size"):
            if arg in ('100KB', '1MB', '10MB'):
                job['s'] = arg
            else:
                print(help)
                sys.exit(4)
    if job['s'] == '' or job['l'] == '' or job['b'] == '':
        print (help)
        sys.exit(3)

    # start_profiling(job)
    # LOOP FROM SCRIPT (SINGLE COMPUTER), REMOVE FOR SLURM USAGE
    for b in range(1024, 481024 + 1, 96000):
       for l in range(0, 8 + 1, 1):
            for s in ('1MB', '10MB'):
                for p in (True, False):
                    job = {'l': str(l), 'b': str(b), 's': s, 'profiler': p}
                    start_profiling(job)
    print("Done iterating")


if __name__ == "__main__":
   main(sys.argv[1:])