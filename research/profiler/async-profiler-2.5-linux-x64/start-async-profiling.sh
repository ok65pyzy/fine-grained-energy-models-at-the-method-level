#!/bin/bash
currentDate=`date +%s`
java -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -cp kanzi-1.9.0.jar kanzi.app.Kanzi -c -f -i sample_file_100KB -l 2 -b 10240 &
processId=`pgrep -n java` && ./profiler.sh -e cpu -i 1 -j 1 -o traces -f traces-${currentDate}.txt -I kanzi.* ${processId}