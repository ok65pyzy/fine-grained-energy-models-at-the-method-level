#!/bin/python3
out = ""
count = 0
for b in range(1024, 102400 + 1, 6400):
    for l in range(0, 8 + 1, 1):
        out += str(count) + " mkdir l_" + str(l) + "_b_" + str(b) + " && cd l_" + str(l) + "_b_" + str(b) + " && java -javaagent:../agent-1.1.jar -cp ../kanzi-1.9.0.jar kanzi.app.Kanzi -c -f -i ../sample_file_10MB -l " + str(l) + " -b " + str(b) + "\n"
        count += 1
print(out)
