#!/bin/python3
import pandas as pd
import time
import multiprocessing
import traceback
import subprocess

# Configuration
CPU_CORES = 8
SHEETS = 144


def process_csv(i):
    log = pd.DataFrame()
    log = log.append({'i': i, 'time': time.time(),
                      'status': 'entering'}, ignore_index=True)
    log.to_csv('log/log_' + str(i) + '.csv',
               encoding='utf-8', index=False)
    target = pd.DataFrame()
    errors = pd.DataFrame()
    try:
        file = 'Call_Tracer_' + str(i) + '.csv'
        df = pd.read_csv(file)

        for index, row in df[df["Exit"] == False].iterrows():
            end = df[(df.Exit == True) & (df.Method == row.Method) & (
                df["Time (microseconds)"] >= row["Time (microseconds)"])].head(1)
            if not end.empty:
                newMethod = {"method": row["Method"].split(
                    "(")[0], "start": row["Time (microseconds)"], "end": end["Time (microseconds)"].values[0]}
                target = target.append(newMethod, ignore_index=True)
            else:
                errors = errors.append({'error': row}, ignore_index=True)
    except FileNotFoundError:
        errors = errors.append(
            {'error': "FileNotFoundError: " + file}, ignore_index=True)
    except:
        errors = errors.append(
            {'error': traceback.format_exc()}, ignore_index=True)

    target.to_csv('out/methods_' + str(i) + '.csv',
                  encoding='utf-8', index=False)
    errors.to_csv('errors/methods_errors_' + str(i) + '.csv',
                  encoding='utf-8', index=False)
    log = log.append({'i': i, 'time': time.time(),
                      'status': 'exiting'}, ignore_index=True)
    log.to_csv('log/log_' + str(i) + '.csv',
               encoding='utf-8', index=False)


def transform_worker(queue):
    while True:
        job = queue.get(True)
        if job is None:
            sys.exit(0)
        else:
            process_csv(job)


def main():
    global CPU_CORES
    global SHEETS

    queue = multiprocessing.Queue()
    pool = multiprocessing.Pool(CPU_CORES, transform_worker, (queue,))

    for i in range(0, SHEETS):
        queue.put(i)

    queue.close()
    queue.join_thread()

    pool.close()
    pool.join()


if __name__ == '__main__':
    main()
