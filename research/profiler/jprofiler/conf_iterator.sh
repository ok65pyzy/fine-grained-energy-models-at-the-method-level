#!/bin/bash
echo "profiling and saving snapshots with jprofiler"
for b in {48000..2592000..48000}
  do
    for l in {0..8..1}
      do
      java -agentpath:/usr/bin/jprofiler12/bin/linux-armhf/libjprofilerti.so=port=8849,offline,id=103,config=jprofiler_config_HotSpots.xml -classpath /usr/bin/jars/kanzi-1.9.0.jar kanzi.app.Kanzi -c -i sample_file -f -l $l -b $b
      done
  done
echo "...done"