#!/bin/python3
import pandas as pd
import time
import multiprocessing
import traceback
import subprocess
import sys

# Configuration
CPU_CORES = 1


def jprofiler_worker(job):
    bash_cmd = ["C:\\Program Files (x86)\\Java\\jre1.8.0_281\\bin\\java.exe", "-agentpath:C:\\Program Files\\jprofiler12\\bin\\windows\\jprofilerti.dll=port=8849,offline,id=103,config=C:\\Users\\oli\\dev\\profiler\\jprofiler_config_CallTracer.xml",
                "-classpath", "C:\\Users\\oli\\dev\\kanzi-master\\java\\target\\kanzi-1.9.0.jar", "kanzi.app.Kanzi", "-c", "-i", "sample_file_100KB", "-f", "-l", str(job['l']), "-b", str(job['b'])]
    return subprocess.call(bash_cmd, shell=False)


def main():
    global CPU_CORES

    tasks = []
    for l in range(1):
        for b in range(1024, 4096 + 1, 512):
            tasks.append({'b': b, 'l': l})

    print("Did", str(len(tasks)), " jProfiler tasks")

    pool = multiprocessing.Pool(processes=CPU_CORES)

    r = pool.map_async(jprofiler_worker, tasks)
    r.wait()
    sys.exit(0)


if __name__ == '__main__':
    main()
