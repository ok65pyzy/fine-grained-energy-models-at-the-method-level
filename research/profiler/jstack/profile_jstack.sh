#!/bin/bash

pid=""
until [ -n "$pid" ]
do
  echo Counter: $pid
  pid=`pgrep -n java`
done
now=`date +%s`
until [ -z "$pid" ]
do
  sudo jstack $pid >> dump-$now.txt
  pid=`pgrep -n java`
done