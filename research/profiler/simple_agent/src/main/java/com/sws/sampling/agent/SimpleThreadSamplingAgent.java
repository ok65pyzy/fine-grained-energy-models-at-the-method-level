package com.sws.sampling.agent;

import java.lang.instrument.*;
import java.util.concurrent.CompletableFuture;

public class SimpleThreadSamplingAgent {

    public static void premain(String argument,
                               Instrumentation instrumentation) {
        System.out.println("currentTimeMillis=" + System.currentTimeMillis());

        CompletableFuture.supplyAsync(Sampler::sample);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("currentTimeMillis=" + System.currentTimeMillis());
        }));
    }
}