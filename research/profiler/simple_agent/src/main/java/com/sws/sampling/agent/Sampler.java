package com.sws.sampling.agent;

import java.util.Map;

public class Sampler {

    public static int sample() {
        while (true) {
            long now = System.nanoTime();
            Map<Thread, StackTraceElement[]> threads = Thread.getAllStackTraces();
            for (Thread t : threads.keySet()) {
                if (t.getName().equals("main")) {
                    StackTraceElement[] traces = threads.get(t);
                    for (int i = 0; i < traces.length; i++) {
                        System.out.println("$trace;" + now + ";" + threads.get(t)[i].toString());
                    }
                    break;
                }
            }
        }
    }
}