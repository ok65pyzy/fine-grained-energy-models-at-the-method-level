# Javaagent for sampling based measuring method execution time

- Compile with 
    ```
    gradle build
    ```
- Use the `agent-1.1.jar` from `/build/libs/`:
    ```
     java -javaagent:agent-1.1.jar -cp <YOUR_JAR>.jar <YOUR_MAIN_CLASS> [...YOUR_PARAMETERS] > method_trace.log
    ```
- Output goes to `method_trace.log` / standard output
