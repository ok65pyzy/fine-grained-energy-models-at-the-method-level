#!/bin/bash

pid=""
until [ -n "$pid" ]
do
  pid=`pgrep -n java`
done
now=`date +%s`
until [ -z "$pid" ]
do
  sudo jcmd kanzi.app.Kanzi Thread.print >> dump-$now.txt
  pid=`pgrep -n java`
done