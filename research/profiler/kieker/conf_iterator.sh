#!/bin/bash
echo "profiling and saving snapshots with kieker"
for b in {48000..2592000..48000}
  do
    for l in {0..8..1}
      do
      java -javaagent:/usr/bin/jars/kieker/kieker-1.16-SNAPSHOT-aspectj.jar -classpath /usr/bin/jars/kanzi-1.9.0.jar kanzi.app.Kanzi -c -i ../jprofiler/sample_file -f -l $l -b $b
      done
  done
echo "...done"