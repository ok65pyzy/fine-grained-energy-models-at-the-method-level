@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  trace-analysis startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and TRACE_ANALYSIS_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\trace-analysis-1.15.jar;%APP_HOME%\lib\kieker-tools-1.15.jar;%APP_HOME%\lib\jgraphx-2.1.1.0.jar;%APP_HOME%\lib\Rsession-2012-09-04.r114.jar;%APP_HOME%\lib\Rserve-0.6-1.jar;%APP_HOME%\lib\REngine-0.6-1.jar;%APP_HOME%\lib\jetty-alpn-openjdk8-server-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-unixsocket-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-gcloud-session-manager-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-schemas-3.1.jar;%APP_HOME%\lib\jetty-servlets-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-jaas-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-server-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-http-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-util-ajax-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-webapp-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-hazelcast-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-cdi-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-servlet-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-proxy-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-alpn-conscrypt-server-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-quickstart-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-alpn-java-server-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-continuation-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-xml-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-memcached-sessions-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-plus-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-rewrite-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-jndi-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-alpn-server-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-openid-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-util-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-deploy-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-jmx-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-nosql-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-security-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-client-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-jaspi-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-annotations-9.4.28.v20200408.jar;%APP_HOME%\lib\jetty-io-9.4.28.v20200408.jar;%APP_HOME%\lib\kieker-analysis-1.15.jar;%APP_HOME%\lib\kieker-model-1.15.jar;%APP_HOME%\lib\kieker-monitoring-1.15.jar;%APP_HOME%\lib\disl-server.jar;%APP_HOME%\lib\kieker-common-1.15.jar;%APP_HOME%\lib\logback-classic-1.1.7.jar;%APP_HOME%\lib\activemq-core-5.7.0.jar;%APP_HOME%\lib\amqp-client-4.2.1.jar;%APP_HOME%\lib\teetime-3.0.jar;%APP_HOME%\lib\oshi-core-3.12.2.jar;%APP_HOME%\lib\cxf-bundle-minimal-2.7.18.jar;%APP_HOME%\lib\kahadb-5.7.0.jar;%APP_HOME%\lib\ehcache-core-2.5.1.jar;%APP_HOME%\lib\slf4j-api-1.7.30.jar;%APP_HOME%\lib\groovy-testng-3.0.2.jar;%APP_HOME%\lib\testng-6.14.3.jar;%APP_HOME%\lib\jcommander-1.72.jar;%APP_HOME%\lib\jackson-core-2.12.3.jar;%APP_HOME%\lib\jackson-annotations-2.12.3.jar;%APP_HOME%\lib\blueprints-core-2.6.0.jar;%APP_HOME%\lib\jackson-databind-2.12.3.jar;%APP_HOME%\lib\commons-math3-3.6.1.jar;%APP_HOME%\lib\commons-lang3-3.12.0.jar;%APP_HOME%\lib\commons-cli-1.4.jar;%APP_HOME%\lib\javax.jms-api-2.0.1.jar;%APP_HOME%\lib\javax.servlet-api-4.0.1.jar;%APP_HOME%\lib\org.eclipse.emf.ecore.xmi-2.16.0.jar;%APP_HOME%\lib\org.eclipse.emf.ecore-2.24.0.jar;%APP_HOME%\lib\org.eclipse.emf.common-2.22.0.jar;%APP_HOME%\lib\guava-23.6-jre.jar;%APP_HOME%\lib\xz-1.9.jar;%APP_HOME%\lib\commons-compress-1.20.jar;%APP_HOME%\lib\nanohttpd-2.3.1.jar;%APP_HOME%\lib\jaxb-api-2.3.1.jar;%APP_HOME%\lib\jctools-core-3.3.0.jar;%APP_HOME%\lib\aspectjrt-1.9.7.jar;%APP_HOME%\lib\aspectjweaver-1.9.7.jar;%APP_HOME%\lib\jersey-server-3.0.2.jar;%APP_HOME%\lib\jersey-client-3.0.2.jar;%APP_HOME%\lib\jersey-common-3.0.2.jar;%APP_HOME%\lib\jakarta.xml.bind-api-2.3.2.jar;%APP_HOME%\lib\javax.jws-api-1.1.jar;%APP_HOME%\lib\spring-jms-3.0.7.RELEASE.jar;%APP_HOME%\lib\spring-tx-3.0.7.RELEASE.jar;%APP_HOME%\lib\aopalliance-1.0.jar;%APP_HOME%\lib\spring-webmvc-5.3.8.jar;%APP_HOME%\lib\spring-context-5.3.8.jar;%APP_HOME%\lib\spring-web-5.3.8.jar;%APP_HOME%\lib\spring-aop-5.3.8.jar;%APP_HOME%\lib\spring-beans-5.3.8.jar;%APP_HOME%\lib\spring-expression-5.3.8.jar;%APP_HOME%\lib\spring-core-5.3.8.jar;%APP_HOME%\lib\logback-core-1.1.7.jar;%APP_HOME%\lib\groovy-ant-3.0.2.jar;%APP_HOME%\lib\groovy-astbuilder-3.0.2.jar;%APP_HOME%\lib\groovy-cli-picocli-3.0.2.jar;%APP_HOME%\lib\groovy-groovysh-3.0.2.jar;%APP_HOME%\lib\groovy-console-3.0.2.jar;%APP_HOME%\lib\groovy-datetime-3.0.2.jar;%APP_HOME%\lib\groovy-groovydoc-3.0.2.jar;%APP_HOME%\lib\groovy-docgenerator-3.0.2.jar;%APP_HOME%\lib\groovy-jmx-3.0.2.jar;%APP_HOME%\lib\groovy-json-3.0.2.jar;%APP_HOME%\lib\groovy-jsr223-3.0.2.jar;%APP_HOME%\lib\groovy-macro-3.0.2.jar;%APP_HOME%\lib\groovy-nio-3.0.2.jar;%APP_HOME%\lib\groovy-servlet-3.0.2.jar;%APP_HOME%\lib\groovy-sql-3.0.2.jar;%APP_HOME%\lib\groovy-swing-3.0.2.jar;%APP_HOME%\lib\groovy-templates-3.0.2.jar;%APP_HOME%\lib\groovy-test-3.0.2.jar;%APP_HOME%\lib\groovy-test-junit5-3.0.2.jar;%APP_HOME%\lib\groovy-xml-3.0.2.jar;%APP_HOME%\lib\groovy-3.0.2.jar;%APP_HOME%\lib\geronimo-jms_1.1_spec-1.1.1.jar;%APP_HOME%\lib\activemq-protobuf-1.1.jar;%APP_HOME%\lib\mqtt-client-1.3.jar;%APP_HOME%\lib\geronimo-j2ee-management_1.1_spec-1.0.1.jar;%APP_HOME%\lib\commons-net-3.1.jar;%APP_HOME%\lib\jasypt-1.9.0.jar;%APP_HOME%\lib\junit-4.13.1.jar;%APP_HOME%\lib\hppc-0.7.3.jar;%APP_HOME%\lib\hamcrest-library-1.3.jar;%APP_HOME%\lib\javax.activation-api-1.2.0.jar;%APP_HOME%\lib\jettison-1.3.3.jar;%APP_HOME%\lib\commons-configuration-1.6.jar;%APP_HOME%\lib\wss4j-1.6.19.jar;%APP_HOME%\lib\opensaml-2.6.1.jar;%APP_HOME%\lib\openws-1.5.1.jar;%APP_HOME%\lib\xmltooling-1.4.1.jar;%APP_HOME%\lib\xmlsec-1.5.8.jar;%APP_HOME%\lib\commons-logging-1.1.1.jar;%APP_HOME%\lib\jna-platform-5.2.0.jar;%APP_HOME%\lib\jna-5.2.0.jar;%APP_HOME%\lib\woodstox-core-asl-4.4.1.jar;%APP_HOME%\lib\stax2-api-3.1.4.jar;%APP_HOME%\lib\xmlschema-core-2.1.0.jar;%APP_HOME%\lib\geronimo-javamail_1.4_spec-1.7.1.jar;%APP_HOME%\lib\wsdl4j-1.6.3.jar;%APP_HOME%\lib\jaxb-impl-2.2.6.jar;%APP_HOME%\lib\jetty-security-8.1.15.v20140411.jar;%APP_HOME%\lib\jetty-server-8.1.15.v20140411.jar;%APP_HOME%\lib\jetty-continuation-8.1.15.v20140411.jar;%APP_HOME%\lib\jetty-http-8.1.15.v20140411.jar;%APP_HOME%\lib\jetty-io-8.1.15.v20140411.jar;%APP_HOME%\lib\jetty-util-8.1.15.v20140411.jar;%APP_HOME%\lib\geronimo-servlet_3.0_spec-1.0.jar;%APP_HOME%\lib\spring-asm-3.0.7.RELEASE.jar;%APP_HOME%\lib\jaxb-xjc-2.2.6.jar;%APP_HOME%\lib\commons-lang-2.6.jar;%APP_HOME%\lib\msv-core-2011.1.jar;%APP_HOME%\lib\xsdlib-2010.1.jar;%APP_HOME%\lib\isorelax-20030108.jar;%APP_HOME%\lib\relaxngDatatype-20020414.jar;%APP_HOME%\lib\xml-resolver-1.2.jar;%APP_HOME%\lib\asm-3.3.1.jar;%APP_HOME%\lib\geronimo-jaxws_2.2_spec-1.1.jar;%APP_HOME%\lib\xmlbeans-2.6.0.jar;%APP_HOME%\lib\joda-time-2.2.jar;%APP_HOME%\lib\serializer-2.7.1.jar;%APP_HOME%\lib\neethi-3.0.3.jar;%APP_HOME%\lib\javax.ws.rs-api-2.0-m10.jar;%APP_HOME%\lib\jakarta.ws.rs-api-3.0.0.jar;%APP_HOME%\lib\jakarta.annotation-api-2.0.0.jar;%APP_HOME%\lib\jakarta.inject-api-2.0.0.jar;%APP_HOME%\lib\jakarta.validation-api-3.0.0.jar;%APP_HOME%\lib\osgi-resource-locator-1.0.3.jar;%APP_HOME%\lib\jakarta.activation-api-1.2.1.jar;%APP_HOME%\lib\spring-jcl-5.3.8.jar;%APP_HOME%\lib\ant-junit-1.10.7.jar;%APP_HOME%\lib\ant-1.10.7.jar;%APP_HOME%\lib\ant-launcher-1.10.7.jar;%APP_HOME%\lib\ant-antlr-1.10.7.jar;%APP_HOME%\lib\picocli-4.1.4.jar;%APP_HOME%\lib\qdox-1.12.1.jar;%APP_HOME%\lib\javaparser-core-3.15.13.jar;%APP_HOME%\lib\jline-2.14.6.jar;%APP_HOME%\lib\junit-jupiter-engine-5.6.0.jar;%APP_HOME%\lib\junit-platform-engine-1.6.0.jar;%APP_HOME%\lib\junit-jupiter-api-5.6.0.jar;%APP_HOME%\lib\junit-platform-commons-1.6.0.jar;%APP_HOME%\lib\junit-platform-launcher-1.6.0.jar;%APP_HOME%\lib\hawtdispatch-transport-1.11.jar;%APP_HOME%\lib\hawtbuf-1.9.jar;%APP_HOME%\lib\jsr305-1.3.9.jar;%APP_HOME%\lib\checker-compat-qual-2.0.0.jar;%APP_HOME%\lib\error_prone_annotations-2.1.3.jar;%APP_HOME%\lib\j2objc-annotations-1.1.jar;%APP_HOME%\lib\animal-sniffer-annotations-1.14.jar;%APP_HOME%\lib\hamcrest-core-1.3.jar;%APP_HOME%\lib\stax-api-1.0.1.jar;%APP_HOME%\lib\commons-collections-3.2.1.jar;%APP_HOME%\lib\commons-digester-1.8.jar;%APP_HOME%\lib\commons-beanutils-core-1.8.0.jar;%APP_HOME%\lib\not-yet-commons-ssl-0.3.9.jar;%APP_HOME%\lib\commons-httpclient-3.1.jar;%APP_HOME%\lib\bcprov-jdk15-1.46.jar;%APP_HOME%\lib\hawtdispatch-1.11.jar;%APP_HOME%\lib\commons-beanutils-1.7.0.jar;%APP_HOME%\lib\javax.servlet-3.0.0.v201112011016.jar;%APP_HOME%\lib\opentest4j-1.2.0.jar


@rem Execute trace-analysis
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %TRACE_ANALYSIS_OPTS%  -classpath "%CLASSPATH%" kieker.tools.trace.analysis.TraceAnalysisToolMain %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable TRACE_ANALYSIS_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%TRACE_ANALYSIS_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
