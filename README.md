# Fine Grained Energy Models At The Method Level

Bachelor Thesis, 07.03.2022



#### Replicate the experiment

- Copy the `experiment/` directory to your home directory

- Change your user name in the following 3 files (replace all occurences of `kronfeld` with your Linux user name)
  - `~/experiment/curie01/slurm_bench_curie.sh`
  - `~/experiment/curie01/slurm_bench_processing.sh`
  - `~/experiment/material/process_measurements.py`

- Execute the script `~/experiment/curie01/submit_curie01.sh` 
  - Now 140 configurations of Kanzi are started and measured (takes about 6 hours)
- When the measurements are finished, set the output directory of these measurements (`measurements_dir`) in `modeling/fgem/learning.py` 
  - e.g. `measurements_dir = '/home/kronfeld/measurements/curie01/884562/'`
- Run the learning script: `python3 modeling/fgem/learning.py`
  - This will learn and print the models (tab formatted to stdout)
  - and will produce the statistical figures in the directory `modeling/fgem/figures`.

#### Simple Agent

The lightweight sampling profiler "Simple Agent" is located at `research/profiler/simple_agent`
